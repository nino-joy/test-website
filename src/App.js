import logo from "./logo.svg";
import "./App.css";

function App() {
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<p>Hello World!</p>
				<a
					className="App-link"
					href="https://google.co.in"
					target="_blank"
					rel="noopener noreferrer"
				>
					Google
				</a>
				<p>Happy</p>
			</header>
		</div>
	);
}

export default App;
